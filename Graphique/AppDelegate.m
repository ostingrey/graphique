//
//  AppDelegate.m
//  Graphique
//
//  Created by Andrew Grabowski on 13.07.2020.
//  Copyright © 2020 Andrew Grabowski. All rights reserved.
//

#import "AppDelegate.h"
#import "EquationEntryViewController.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSSplitView *horizontalSplitView;
@property (weak) IBOutlet NSSplitView *verticalSplitView;
@property (strong) EquationEntryViewController *equationEntryViewController;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    self.equationEntryViewController = [[EquationEntryViewController alloc]
                                        initWithNibName:@"EquationEntryViewController" bundle:nil];
    [self.verticalSplitView replaceSubview:[[self.verticalSplitView subviews] objectAtIndex:1]
                                      with: _equationEntryViewController.view];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    //
}

- (void)applicationDidResignActive:(NSNotification *)notification {
    //
}
@end
