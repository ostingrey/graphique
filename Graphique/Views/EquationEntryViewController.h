//
//  EquationEntryViewController.h
//  Graphique
//
//  Created by Andrew Grabowski on 13.07.2020.
//  Copyright © 2020 Andrew Grabowski. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface EquationEntryViewController : NSViewController

@end

NS_ASSUME_NONNULL_END
