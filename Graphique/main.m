//
//  main.m
//  Graphique
//
//  Created by Andrew Grabowski on 13.07.2020.
//  Copyright © 2020 Andrew Grabowski. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
